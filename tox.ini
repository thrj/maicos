[tox]
minversion = 4.0.11
# these are the environments that will run when you
# execute `tox` in the command-line
# bellow you will find explanations for all environments
envlist =
    py{38,39,310,311}-tests
    py{38,39,310,311}-build-{linux,macos,windows}
    docs
    lint

# configures which environments run with each python version
[testenv]
platform =
    linux: linux
    macos: darwin
    windows: win32

# Tox will not pass the environment variables to the packaging step, so we need
# to explicitly set them here. (see https://github.com/tox-dev/tox/issues/2543)
[testenv:.pkg]
setenv =
    USE_CYTHON = True

# configures the unittest environment
[testenv:py{38,39,310,311}-tests]
setenv =
    PYTHONPATH={toxinidir}/tests
    PYTHONUNBUFFERED=yes
usedevelop = true
# installs dependencies we need for testing
# by using tox the developer don't need to manage this dependencies
# him/herself
extras = tests
# before running the tests erases any prerecord of coverage
commands_pre =
    coverage erase
commands =
    # Run unit tests
    pytest --cov --cov-report=term-missing --cov-append \
           --cov-config=.coveragerc --hypothesis-show-statistics {posargs}

    # Run documentation tests
    pytest --doctest-modules --pyargs maicos {posargs}

# after executing the pytest assembles the coverage reports
commands_post =
    coverage report
    coverage html
    coverage xml

# separates lint from build env
[testenv:lint]
deps =
    flake8>=4
    flake8-docstrings
    flake8-bugbear
    flake8-sphinx-links
    pygments
    isort
skip_install = true
commands =
    flake8 {posargs:src/maicos examples tests setup.py}
    isort --verbose --check-only --diff src/maicos examples tests setup.py

# asserts package build integrity
[testenv:py{38,39,310,311}-build-{linux,macos,windows}]
usedevelop = true
extras = dev
deps =
    build
    check-manifest
    twine
allowlist_externals =
    linux: bash
    macos: zsh
    windows: foreach

commands_pre = python {toxinidir}/developer/clean_dist_check.py
commands =
    python -m build
    twine check dist/*.tar.gz dist/*.whl
    check-manifest {toxinidir}
    # Workaround using shells since tox does not support wildcards
    linux: bash -c "python -m pip install --force-reinstall dist/maicos-*.tar.gz"
    linux: bash -c "python -m pip install --force-reinstall dist/maicos-*.whl"
    # Disabled for now because it doesn't work on the gitlab ci for whatever reasons...
    # macos: zsh -c "python -m pip install --force-reinstall dist/maicos-*.tar.gz"
    # macos: zsh -c "python -m pip install --force-reinstall dist/maicos-*.whl"
    # windows: foreach ($i in ls dist\*.tar.gz) \{pip install --force-reinstall $i\}
    # windows: foreach ($i in ls dist\*.whl) \{pip install --force-reinstall $i\}

# code quality assessment. This is not a check in the CI, serves just
# as info for the developer
[testenv:radon]
deps = radon
skip_install = true
commands =
    radon cc -s --total-average --no-assert {posargs:src/maicos}
    radon mi -m -s {posargs:src/maicos}

# Simulate docs building as it will occur on ReadTheDocs
# if this fails, most likely RTD build will fail
[testenv:docs]
usedevelop = true
extras = docs
deps = gitpython
commands =
    sphinx-build {posargs:-E} -W -b html docs/src dist/docs
    python '{toxinidir}/developer/check_changelog.py'

# safety checks
[testenv:safety]
deps = safety
skip_install = true
commands = safety check

# my favourite configuration for flake8 styling
# https://flake8.pycqa.org/en/latest/#
[flake8]
max_line_length = 80
hang-closing = true
ignore =
    D401
    W503
docstring-convention = numpy
exclude =
    src/maicos/__init__.py
    src/maicos/_version.py
    versioneer.py
per-file-ignores =
    # D205 and D400 are incompatible with the requirements of sphinx-gallery
    examples/*:D205, D400
