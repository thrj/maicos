MAICoS' Documentation
=====================

.. image:: ../static/logo_MAICOS_gray.png
   :alt: MAICoS logo

This documentation covers everything you need to know about MAICoS,
the Molecular Analysis for Interfacial and Confined Systems toolkit.
There are five sections:

- :ref:`userdoc-get-started`
- :ref:`userdoc-how-to`
- :ref:`userdoc-references`
- :ref:`userdoc-explanations`
- :ref:`devdoc`

If you are new to MAICoS, we recommend starting with the
:ref:`userdoc-get-started` section. If you want to contribute to the development
of the library, please have a look at our :ref:`developer documentation
<devdoc>`.


Getting started
---------------

The :ref:`userdoc-get-started` section is for MAICoS beginners. It will
help you install and familiarize yourself with MAICoS and its approach
to analyse molecular dynamics simulations.

How-to guides
-------------

The :ref:`userdoc-how-to` section is for MAICoS intermediate and
advanced users. It contains guides taking you through series of
steps involved in addressing key problems and use-cases in MAICoS.

Reference guides
----------------

The :ref:`userdoc-references` section is for MAICoS intermediate and advanced users,
it contains technical references and parameter list for each MAICoS modules
(:ref:`userdoc-how-to`) as well as the APIs. It describes the various functionalities
provided by MAICoS. You can always refer to this section to learn more about
classes, functions, modules, and other aspects of MAICoS machinery you may come across.

Explanations
------------

The :ref:`userdoc-explanations` section discusses key topics and concepts at a fairly high level
and provides explanations to expand your knowledge of MAICoS. It
requires at least basic to intermediate knowledge of MAICoS.

Developer documentation
-----------------------

The :ref:`devdoc` helps you contributing to the code base
or the documentation of MAICoS.

.. toctree::
   :hidden:

   get-started/index
   how-to/index
   references/index
   explanations/index
   devdoc/index
