.. _VelocityPlanar:

VelocityPlanar
##############

.. autoclass:: maicos.modules.velocityplanar.VelocityPlanar
    :members:
    :undoc-members:
    :show-inheritance:
