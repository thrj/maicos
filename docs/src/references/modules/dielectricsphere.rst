.. _DielectricSphere:

DielectricSphere
##################

.. autoclass:: maicos.modules.dielectricsphere.DielectricSphere
    :members:
    :undoc-members:
    :show-inheritance:
