..
    Names are in alphabetical order

Maintainers
-----------

- Philip Loche
- Henrik Jaeger
- Alexander Schlaich

Developers
----------

- Maximilian Becker
- Simon Gravelle
- Philipp Stärk
- Srihas Velpuri

Contributors
------------

- Shane Carlson
- Kira Fischer
- Julian Kappler
- Marc Sauter
- Laura Scalfi
- Julius Schulz
- Dominik Wille
- Amanuel Wolde-Kidan
