.. _core_classes:

============
Core classes
============

.. automodule:: maicos.core

.. toctree::
   :titlesonly:
   :maxdepth: 4
   :caption: Core classes
   
   base
   planar
   cylinder
   sphere
