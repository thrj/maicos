.. _RDFPlanar:

RDFPlanar
#########

.. autoclass:: maicos.modules.rdfplanar.RDFPlanar
    :members:
    :undoc-members:
    :show-inheritance:
