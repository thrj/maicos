.. _devdoc:

Developer documentation
#######################

.. include:: ../../../CONTRIBUTING.rst

.. toctree::
    :maxdepth: 0

    ../examples/own_module
    tests
    documentation
    versioning
    release