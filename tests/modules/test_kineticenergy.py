#!/usr/bin/env python3
# -*- Mode: python; tab-width: 4; indent-tabs-mode:nil; coding:utf-8 -*-
#
# Copyright (c) 2022 Authors and contributors
# (see the AUTHORS.rst file for the full list of names)
#
# Released under the GNU Public Licence, v3 or any higher version
# SPDX-License-Identifier: GPL-3.0-or-later
"""Tests for the KineticEnergy class."""
import os
import sys

import MDAnalysis as mda
import numpy as np
import pytest
from create_mda_universe import line_of_water_molecules
from numpy.testing import (
    assert_allclose,
    assert_almost_equal,
    assert_equal,
    assert_raises,
    )

from maicos import KineticEnergy


sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
from data import NVE_WATER_TPR, NVE_WATER_TRR  # noqa: E402


class ReferenceAtomGroups:
    """Super class with methods reference AtomGroups for tests."""

    @pytest.fixture()
    def ag(self):
        """Import MDA universe."""
        u = mda.Universe(NVE_WATER_TPR, NVE_WATER_TRR)
        return u.atoms


class TestKineticEnergy(ReferenceAtomGroups):
    """Tests for the KineticEnergy class."""

    def test_ke_trans_trajectory(self, ag):
        """Test translational kinetic energy."""
        ke = KineticEnergy(ag, refpoint="COM").run(stop=1)
        assert_allclose(ke.results.trans, 1905.26, rtol=1e-2)

    def test_ke_trans_trajectory_save(self, ag, tmpdir):
        """
        Test translational kinetic energy.

        Save the result in a text file, and assert that the
        results printed in the file is correct.
        """
        with tmpdir.as_cwd():
            ke = KineticEnergy(ag, refpoint="COM").run(stop=1)
            ke.save()
            assert_equal(os.path.exists("ke.dat"), True)
            saved = np.loadtxt("ke.dat")
            assert_almost_equal(saved[1], 1905.26, decimal=2)

    def test_ke_rot(self, ag):
        """Test rotational kinetic energy."""
        ke = KineticEnergy(ag).run(stop=1)
        assert_almost_equal(ke.results.rot, 1898.81, decimal=2)

    def test_prepare(self, ag):
        """Test Value error when refpoint is not COM or COC."""
        kem = KineticEnergy(ag, refpoint="OH")
        with assert_raises(ValueError):
            kem.run()

    def test_ke_rot_COC(self, ag):
        """Test rotational KE COC."""
        ke = KineticEnergy(ag, refpoint="COC").run(stop=1)
        assert_almost_equal(ke.results.rot, 584.17, decimal=1)

    @pytest.mark.parametrize('vel', (0, 1, 2))
    def test_ke_single_molecule(self, vel):
        """
        Test KineticEnergy module using a single molecule.

        Create a universe with one single water molecule
        with a given velocity of vel along z.

        The expected result corresponds to the 0.5*m*v**2 (in kJ/mol)
        where m is the mass of a single water molecule.
        """
        ag = line_of_water_molecules(n_molecules=1, myvel=np.array([0, 0, vel]))
        vol = np.prod(ag.dimensions[:3])
        ke = KineticEnergy(ag, refpoint="COM").run()

        mass_h2o = 18.0153 / 1000  # kg/mol
        myke = 0.5 * mass_h2o * (vel * 100) ** 2  # kJ/mol (100 = A/ps to m/s)

        assert_almost_equal(ke.results.trans, myke / vol, decimal=1)
